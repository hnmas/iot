package com.iteaj.iot.event;

import org.springframework.context.ApplicationListener;

public interface ExceptionEventListener extends ApplicationListener<ExceptionEvent> {

}
