package com.iteaj.iot.client;

import com.iteaj.iot.Protocol;
import com.iteaj.iot.ProtocolException;

/**
 * 协议不可写异常
 */
public class UnWritableProtocolException extends ProtocolException {

    /**
     * @see com.iteaj.iot.Protocol
     * @param protocol
     */
    public UnWritableProtocolException(Object protocol) {
        super(protocol);
    }

    public UnWritableProtocolException(String message, Object protocol) {
        super(message, protocol);
    }

    @Override
    public Protocol getProtocol() {
        return (Protocol) super.getProtocol();
    }
}
