package com.iteaj.iot.test.modbus.dtu;

import com.iteaj.iot.modbus.server.dtu.ModbusRtuMessageAware;
import com.iteaj.iot.server.dtu.DefaultDtuMessageAware;
import io.netty.buffer.ByteBuf;

public class DtuAtTestAware extends ModbusRtuMessageAware {

    public DtuAtTestAware() { }

    @Override
    public boolean isAt(String equipCode, byte[] message, ByteBuf msg) {
        return new String(message).startsWith(equipCode);
    }
}
