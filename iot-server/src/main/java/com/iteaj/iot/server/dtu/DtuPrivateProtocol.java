package com.iteaj.iot.server.dtu;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.server.ServerMessage;
import com.iteaj.iot.server.protocol.ClientInitiativeProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dtu的私有协议
 */
public class DtuPrivateProtocol extends ClientInitiativeProtocol<ServerMessage> {

    private static Logger logger = LoggerFactory.getLogger(DtuPrivateProtocol.class);

    public DtuPrivateProtocol(ServerMessage requestMessage) {
        super(requestMessage);
    }

    @Override
    protected ServerMessage doBuildResponseMessage() {
        return null;
    }

    @Override
    protected void doBuildRequestMessage(ServerMessage requestMessage) {
        if(logger.isDebugEnabled()) {
            logger.debug("DTU设备 DTU私有协议 - 设备编号: {} - 报文: {}", requestMessage.getHead().getEquipCode(), requestMessage);
        }
    }

    @Override
    public ProtocolType protocolType() {
        return DtuCommonProtocolType.DTU;
    }
}
