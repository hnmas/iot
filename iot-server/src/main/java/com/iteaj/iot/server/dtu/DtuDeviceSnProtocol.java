package com.iteaj.iot.server.dtu;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.server.ServerMessage;
import com.iteaj.iot.server.protocol.ClientInitiativeProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DtuDeviceSnProtocol extends ClientInitiativeProtocol<ServerMessage> {

    private static Logger logger = LoggerFactory.getLogger(DtuDeviceSnProtocol.class);

    public DtuDeviceSnProtocol(ServerMessage requestMessage) {
        super(requestMessage);
    }

    @Override
    protected ServerMessage doBuildResponseMessage() {
        return null;
    }

    @Override
    protected void doBuildRequestMessage(ServerMessage requestMessage) {
        if(logger.isDebugEnabled()) {
            logger.trace("DTU设备 注册设备编号 - 设备编号: {}", requestMessage.getHead().getEquipCode());
        }
    }

    @Override
    public ProtocolType protocolType() {
        return DtuCommonProtocolType.DEVICE_SN;
    }
}
