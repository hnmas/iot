package com.iteaj.iot.modbus.server.dtu;

import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.server.dtu.DtuMessageAware;
import com.iteaj.iot.server.dtu.SimpleChannelForDtuDecoderComponent;

/**
 * 适用于：对于Dtu连网的且设备使用Modbus Rtu协议进行数据交互的情况
 * 注意：使用此解码组件需要需用使用同步的方式操作设备(由于不处理粘包的情况)
 */
public class ModbusRtuForDtuServerComponent<M extends ModbusRtuForDtuMessage> extends SimpleChannelForDtuDecoderComponent<M> {

    public ModbusRtuForDtuServerComponent(ConnectProperties connectProperties) {
        this(connectProperties, new ModbusRtuMessageAware());
    }

    public ModbusRtuForDtuServerComponent(ConnectProperties connectProperties, DtuMessageAware<M> dtuMessageAwareDelegation) {
        super(connectProperties, dtuMessageAwareDelegation);
    }

    @Override
    public String getName() {
        return "ModbusRtuForDtu";
    }

    @Override
    public String getDesc() {
        return "使用Dtu连网且设备基于标准Modbus Rtu协议的iot服务端实现";
    }

    @Override
    public AbstractProtocol doGetProtocol(ModbusRtuForDtuMessage message) {
        return remove(message.getHead().getMessageId());
    }

}
