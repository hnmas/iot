package com.iteaj.iot.modbus.server.dtu;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.modbus.consts.ModbusCode;
import com.iteaj.iot.modbus.consts.ModbusErrCode;
import com.iteaj.iot.server.dtu.DefaultDtuMessageAware;
import com.iteaj.iot.server.dtu.DtuCommonProtocolType;
import com.iteaj.iot.server.dtu.DtuMessageDecoder;
import com.iteaj.iot.utils.ByteUtil;
import io.netty.buffer.ByteBuf;

public class ModbusTcpMessageAware<M extends ModbusTcpForDtuMessage> extends DefaultDtuMessageAware<M> {

    public ModbusTcpMessageAware() { }

    public ModbusTcpMessageAware(DtuMessageDecoder decoder) {
        super(decoder);
    }

    @Override
    protected ProtocolType customizeType(String equipCode, byte[] message, ByteBuf msg) {
        // modbus tcp 协议报文长度不能小于8
        // 小于8说明是dtu的报文
        if(message.length < 8) {
            return DtuCommonProtocolType.DTU;
        }

        // 获取modbus tcp的功能码
        int code = message[7] & 0xFF;
        if(code > 0x80) { // 读取到的功能码可能是modbus的异常码, 异常码大于 0x80
            code = code - 0x80;
            try {
                ModbusCode.INSTANCE((byte) code);
                ModbusErrCode.valueOf(message[8]);

                return null;
            } catch (IllegalStateException e) {
                return DtuCommonProtocolType.DTU;
            }
        }

        // modbus的功能码只能是在 0x01 - 0x10之间
        if(code >= 0x01 && code <= 0x10) {
            try {
                ModbusCode.INSTANCE((byte) code);
                // 判断modbus tcp报文长度字段
                short length = ByteUtil.bytesToShortOfReverse(message, 4);
                // 报文的长度必须 >= modbus tcp的长度
                // 如果报文的长度大于modbus tcp的长度说明modbus tcp的报文和dtu的报文产生了粘包
                if(message.length < length + 6) {
                    return DtuCommonProtocolType.DTU;
                }
            } catch (IllegalStateException e) { // 不是modbus协议的功能码
                return DtuCommonProtocolType.DTU;
            }
        } else { // 不存在功能码说明不是modbus协议
            return DtuCommonProtocolType.DTU;
        }

        return null;
    }
}
